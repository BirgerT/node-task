const readline = require("readline")
const fs = require('fs')
const os = require('os')
const http = require('http')


class App {
    constructor() {
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        })
        this.askQuestion()
    }

    askQuestion() {
        this.rl.question(this.createOptions(),
            (option) => {
                switch (option) {
                    case '1':
                        this.readFile('/package.json')
                        break
                    case '2':
                        this.displayOSInfo()
                        break
                    case '3':
                        this.startServer()
                        break
                    default:
                        console.log('Invalid option.')
                }
                this.rl.close()
            }
        )
    }

    createOptions = () => {
        return `Choose an option:
            1. Read package.json
            2. Display OS info
            3. Start HTTP server
            Type a number: `
            .replace(/  /g, '')
    }

    readFile = (filePath) => {
        console.log(`Reading ${filePath.split('/').pop()}:`)
    
        fs.readFile(__dirname + filePath, 'utf-8', (err, data) => {
            console.log(data)
        })
    }
    
    displayOSInfo = () => {
        console.log(
            `SYSTEM MEMORY: ${this.convertToGB(os.totalmem)}
            FREE MEMORY: ${this.convertToGB(os.freemem)}
            CPU CORES: ${os.cpus().length}
            ARCH: ${os.arch()}
            PLATFORM: ${os.platform()}
            RELEASE: ${os.release()}
            USER: ${os.userInfo().username}`
            .replace(/  /g, ''))
    }
    
    convertToGB = (bytes) => {
        return (bytes / 1024 / 1024 / 1024).toFixed(2) + ' GB'
    }
    
    startServer = () => {
        console.log('Starting HTTP server...')
        const server = http.createServer((req, res) => {
            if (req.url === '/') {
                res.write('Hello World!')
                res.end()
            }
        })
        server.listen(3000, () => {
            console.log('Listening on port 3000...')
        })
    }
}

new App()
